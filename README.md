# Descomplicando o GitLab

Treinamento descomplicando o GitLab criado ao vivo na  Twitch

### Day-1

```bash
- Entendemos o que é o GitLab
- Entendemos o que é Working Dir, Index e HEAD
- Entendemos o que é o GitLab
- Como criar um grupo no GitLab
- Como criar um repositório no Git
- Aprendemos os comandos basicos para manipulação de arquivos e diretórios no git
- Como criar um branch
- Como criar um Merge Request
- Como adicionar um Membro no projeto
- Como fazer um Merge na Master/main
- Como associar um repo local com um repo remoto
- Como impertar um repo do GitHub para o GitLab
- Mudamos a Branch padrão para main
```